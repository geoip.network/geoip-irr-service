from collections import namedtuple
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from datetime import datetime
from functools import lru_cache, partial
from ipaddress import ip_address, ip_network
from os import environ
from random import choices, choice
from time import sleep
from typing import List, Dict, Set, Optional
from urllib.parse import urljoin

import numpy as np
import requests
from icmplib import ping
from scipy.interpolate import griddata, interp1d
from scipy.ndimage import gaussian_filter
from skimage.filters.thresholding import threshold_multiotsu
from skimage.measure import label, regionprops
from skimage.morphology import square, closing
from skimage.segmentation import clear_border

api_auth = None


def get_bearer():
    global api_auth
    headers = {'content-type': "application/x-www-form-urlencoded"}
    payload = {
        "grant_type": "client_credentials",
        "client_id": environ.get("GEOIP_AUTH_ID"),
        "client_secret": environ.get("GEOIP_AUTH_SECRET"),
        "audience": environ.get("GEOIP_API_URL")
    }
    api_auth = requests.post(urljoin(environ.get("GEOIP_AUTH_URL"), "oauth/token"), headers=headers, data=payload).json()


def found():
    result = requests.get(
        urljoin(environ.get('GEOIP_API_URL'), 'v1.0/metrics'),
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    result.raise_for_status()
    jdata = result.json()
    notfound = {search["cidr"]: True for search in jdata["notfound"]}
    candidates = [search["cidr"] for search in jdata["lookedup"] if (search["count"] > 5) and (search["cidr"] not in notfound)]
    return candidates


Target = namedtuple("Target", ["target", "name"])


def create_measurements(probe_ids: List[str], target_set: Set[Target], packets: int):
    request = {
        "definitions": [],
        "probes": [
            {
                "value": ",".join(probe_ids),
                "type": "probes",
                "requested": len(probe_ids)
            }
        ],
        "is_oneoff": True,
        "bill_to": "tim@treestle.com",
    }
    url = f"https://atlas.ripe.net/api/v2/measurements/?key={environ.get('RIPE_KEY')}"
    for target in target_set:
        definition = {
            "target": target.target,
            "protocol": "ICMP",
            "response_timeout": 1000,
            "af": ip_address(target.target).version,  # AF_INET6 == 10 but this interface actually expects IP version
            "packets": packets,
            "size": 48,
            "description": target.name,
            "resolve_on_probe": False,
            "skip_dns_check": True,
            "include_probe_id": True,
            "type": "ping"
        }
        request["definitions"].append(definition)
    try:
        response = requests.post(url=url, json=request)
        return response.json()["measurements"]
    except Exception as e:
        print(e)


@lru_cache(None)
def get_probes(**params):
    probes = {}
    params["page"] = 1
    while (
            result := requests.get(
                "https://atlas.ripe.net/api/v2/probes/",
                params=params
            ).json()
    ).get("next") is not None:
        params["page"] += 1
        for probe in result["results"]:
            if (probe["address_v4"] is not None) and (probe["geometry"] is not None) and (probe["geometry"].get("coordinates") is not None):
                probes[probe["id"]] = probe
    return probes


@dataclass
class Analysis:
    mean: Optional[float] = None
    mode: Optional[float] = None
    standard_dev: Optional[float] = None
    min: Optional[float] = None
    max: Optional[float] = None


def get_result(measurement: int, pre_filter=True) -> Dict:
    samples: List[Dict] = requests.get(
        f"https://atlas.ripe.net/api/v2/measurements/{measurement}/results/?format=json"
    ).json()
    meta = requests.get(
        f"https://atlas.ripe.net/api/v2/measurements/{measurement}/?format=json"
    ).json()
    result = {
        "meta": meta,
        "samples": [sample for sample in samples if sample["avg"] != -1] if pre_filter else samples,
        "ready": (datetime.now().timestamp() - meta.get("start_time", int(datetime.now().timestamp()))) > 360
    }
    return result


def get_measurements(measurement_ids, use_ready=True):
    for measurement in measurement_ids:
        if use_ready:
            while ((result := get_result(measurement)) is None) or (not result["ready"]):
                sleep(1)
        else:
            while ((result := get_result(measurement, False)) is None) or (len(result["samples"]) < 1):
                sleep(1)
        yield result


def find_pingable(cidr):
    network = ip_network(cidr)
    potential_targets = [
        sub.network_address.compressed
        for sub in network.subnets(new_prefix=(32 if network.version == 4 else 64))
    ]
    targets = set([Target(target, network.compressed) for target in choices(potential_targets, k=99)])
    probes = get_probes(**{
        "format": "json",
        "is_public": True,
        "is_anchor": True,
        "tags": "system-ipv4-works",
        "status": 1
    })
    probe_ids = [str(probe) for probe in choices(list(probes.keys()), k=1)]
    measurement_ids = create_measurements(probe_ids, targets, 1)
    measurements = get_measurements(measurement_ids, False)
    results = []
    for measurement in measurements:
        if measurement["samples"] and measurement["samples"][0]["rcvd"] == measurement["samples"][0]["sent"] and measurement["samples"][0]["avg"] < 400:
            results.append((measurement["samples"][0]["avg"], measurement["samples"][0]["dst_addr"]))
    results.sort()
    return [result[1] for result in results if result[0]]


def find_pingable2(cidr):
    network = ip_network(cidr)
    targets = [
        sub.network_address.compressed
        for sub in network.subnets(new_prefix=(32 if network.version == 4 else 64))
    ]
    results = []
    with ThreadPoolExecutor(max_workers=8) as executor:
        raw_results = executor.map(partial(ping, count=1, interval=1, timeout=.5), targets)
        for result in raw_results:
            if result.is_alive and (result.packet_loss == 0) and (result.max_rtt < 150):
                results.append((result.max_rtt, result.address))
                print(result)
    results.sort()
    return [result[1] for result in results]


def interpolate(probes, measurements):
    for measurement in measurements:
        clean_samples = [sample for sample in measurement["samples"] if sample["avg"] < 100]
        grid_x, grid_y = np.mgrid[-180:180:1440j, -90:90:720j]
        points = np.ndarray((len(clean_samples), 2))
        values = np.ndarray((len(clean_samples), ))
        for i, sample in enumerate(clean_samples):
            probe = probes[sample["prb_id"]]
            long, lat = probe["geometry"]["coordinates"]
            points[i] = [long, lat]
            values[i] = sample["avg"]
        inverter = interp1d([min(values), max(values)], [1000.0, 0.0])
        values = np.power(inverter(values), 16)
        mapper = interp1d([min(values), max(values)], [0.0, np.power(10, 8)])
        values = mapper(values)
        grid_data_linear = griddata(points, values, (grid_x, grid_y), method='linear', fill_value=0)
        grid_data_linear_smooth = gaussian_filter(grid_data_linear, sigma=5)
        grid_data_linear_smooth = np.power(grid_data_linear_smooth, 16)
        yield grid_data_linear_smooth


@lru_cache(None)
def lookup_cidr(cidr):
    result = requests.get(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{cidr}/internal'),
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


@lru_cache(None)
def select_probes(cidr):
    probes = get_probes(**{
        "format": "json",
        "is_public": True,
        "is_anchor": False,
        "tags": "system-ipv4-works",
        "status": 1
    })
    grid = [[[] for x in range(36)] for y in range(14)]
    for probe in probes.values():
        long, lat = probe["geometry"]["coordinates"]
        long_bucket = int(round(long/10)) + 17
        lat_bucket = int(round(lat/10)) + 6
        if (not (0 <= lat_bucket < 14)) or (not (0 <= long_bucket < 36)):
            continue
        grid[lat_bucket][long_bucket].append(probe)
    probes = {}
    api_response = lookup_cidr(cidr)
    coords = api_response.get("geo", {}).get("geometry", {}).get("coordinates")
    long_bucket = int(round(coords[0] / 10)) + 17
    lat_bucket = int(round(coords[1] / 10)) + 6
    long_min = max(0, long_bucket - 2)
    long_max = min(35, long_bucket + 2)
    lat_min = max(0, lat_bucket - 2)
    lat_max = min(14, lat_bucket + 2)
    while len(probes.keys()) < 500:
        for j in range(lat_min, lat_max):
            for i in range(long_min, long_max):
                if len(grid[j][i]) >= 1:
                    for probe in choices(grid[j][i], k=min(20, len(grid[j][i]))):
                        probes[probe["id"]] = probe
        for y in range(14):
            for x in range(36):
                if len(grid[y][x]) >= 1:
                    probe = choice(grid[y][x])
                    probes[probe["id"]] = probe
    while len(probes.keys()) > 500:
        probe = choice(list(probes.values()))
        del probes[probe["id"]]
    probe_ids = [str(probe["id"]) for probe in probes.values()]
    return probe_ids, probes


def localize(cidr, targets):
    probe_ids, probes = select_probes(cidr)
    measurement_ids = create_measurements(probe_ids, targets, 3)
    measurements = get_measurements(measurement_ids)
    grids = interpolate(probes, measurements)
    location_estimates = []
    radius_estimates = []
    for grid in grids:
        thresholds = threshold_multiotsu(grid, classes=4)
        thresh = thresholds[-1]
        bw = closing(grid > thresh, square(3))
        cleared = clear_border(bw)
        label_img = label(cleared)
        regions = regionprops(label_img)
        for props in regions:
            x0, y0 = props.centroid
            print((x0/4)-180, (y0/4)-90, (props.axis_minor_length/4))
            location_estimates.append([(x0/4)-180, (y0/4)-90])
            radius_estimates.append((props.axis_minor_length/4) / 2)
    xs = np.ndarray((len(location_estimates), 2))
    ys = np.ndarray((len(location_estimates), 2))
    weights = np.ndarray((len(location_estimates), 2))
    max_x = -180
    min_x = 180
    max_y = -90
    min_y = 90
    for i, location in enumerate(location_estimates):
        xs[i] = location[0]
        ys[i] = location[1]
        weights[i] = radius_estimates[i]
        max_x = max(max_x, location[0] + radius_estimates[i], location[0] - radius_estimates[i])
        min_x = min(min_x, location[0] + radius_estimates[i], location[0] - radius_estimates[i])
        max_y = max(max_y, location[1] + radius_estimates[i], location[1] - radius_estimates[i])
        min_y = min(min_y, location[1] + radius_estimates[i], location[1] - radius_estimates[i])
    estimate_x = np.sum(xs*weights)/np.sum(weights)
    estimate_y = np.sum(ys*weights)/np.sum(weights)
    estimate_radius = max(max_x-min_x, max_y-min_y)
    return estimate_x, estimate_y, estimate_radius/2


def filter_candidates(candidate_cidrs):
    response = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), 'v1.0/cidrs/internal'),
        json=[ip_network(cidr).compressed for cidr in candidate_cidrs],
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    results = response.json()
    for result in results:
        print(f"filter validating: {result}")
        if ("error" not in result) and (result["geo"]["properties"]["radius"] == 0.0):
            yield result["cidr"]


def post_cidr(jdata: Dict):
    result = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def put_cidr(jdata: Dict):
    result = requests.put(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def main():
    get_bearer()
    candidates = filter_candidates(found())
    print("got candidates")
    for candidate in candidates:
        cidr = ip_network(candidate)
        print(f"candidate: {cidr}")
        target_size = (22 if (cidr.version == 4) else 48)
        print(f"validation: {cidr.compressed}")
        for subnet in cidr.subnets(new_prefix=target_size):
            subnet_record = lookup_cidr(subnet.compressed)
            if ("error" in subnet_record) or ((subnet_record["cidr"] == subnet.compressed) and (subnet_record["geo"]["properties"]["radius"] != 0)):
                continue
            print(f"pinging: {subnet}")
            pingables = find_pingable2(subnet.compressed)
            if len(pingables) >= 1:
                ips = pingables[:3]
                target_ips = [Target(ip, subnet.compressed) for ip in ips]
                print("localizing")
                estimation = localize(subnet.compressed, target_ips)
                print("updating")
                value = lookup_cidr(subnet.compressed)
                new = {
                    "cidr": subnet.compressed,
                    "asn": value['asn'],
                    "allocated_cc": value["allocated_cc"],
                    "rir": value["rir"],
                    "as-name": value["as-name"],
                    "geo": {
                        "type": "Feature",
                        "properties": {"radius": estimation[2]*111000},  # 111km / degree
                        "geometry": {
                            "coordinates": list(estimation[:2]),
                            "type": "Point"
                        }
                    }
                }
                print(new)
                if value["cidr"] == subnet.compressed:
                    put_cidr(new)
                else:
                    post_cidr(new)
                print(subnet.compressed + " complete")


if __name__ == "__main__":
    main()
