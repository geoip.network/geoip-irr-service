from ipaddress import ip_network
from os import environ
from typing import List, Dict
from urllib.parse import urljoin

import requests


api_auth = None


def get_bearer():
    global api_auth
    headers = {'content-type': "application/x-www-form-urlencoded"}
    payload = {
        "grant_type": "client_credentials",
        "client_id": environ.get("GEOIP_AUTH_ID"),
        "client_secret": environ.get("GEOIP_AUTH_SECRET"),
        "audience": environ.get("GEOIP_API_URL")
    }
    api_auth = requests.post(urljoin(environ.get("GEOIP_AUTH_URL"), "oauth/token"), headers=headers, data=payload).json()


def get_probes():
    data = {
        "format": "json",
        "is_public": True,
        "page": 1,
        "status": 1
    }
    probes = []
    while (
            result := requests.get(
                "https://atlas.ripe.net/api/v2/probes/",
                params=data,
                headers={"Authorization": f"Bearer {api_auth['access_token']}"}
            ).json()
    )["next"] is not None:
        data["page"] += 1
        probes += [probe for probe in result["results"] if probe["address_v4"] is not None]
    return probes


def lookup_cidrs(cidrs: List[str]):
    result = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidrs/internal'),
        json=cidrs,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def lookup_cidr(cidr):
    result = requests.get(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{cidr}/internal'),
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def enhance(probes):
    ips = [probe["address_v4"] for probe in probes]
    for i, result in enumerate(lookup_cidrs(list(ips))):
        probe = probes[i]
        probe["parent"] = result
        if "error" not in result:
            yield probe
        else:
            print(lookup_cidr(probe["address_v4"]))
            print("probe on unknown network")
            print(probe)
            print(result)


def prepare(probes):
    ready = {}
    for value in probes:
        network = ip_network(value["address_v4"]).supernet(8)
        new = {
            "is_put": network.compressed == value["parent"]["cidr"],
            "cidr": network.compressed,
            "asn": f"AS{value['asn_v4']}",
            "allocated_cc": value["country_code"],
            "rir": value["parent"]["rir"],
            "as-name": value["parent"]["as-name"],
            "geo": {
                "type": "Feature",
                "properties": {"radius": 43000.0},
                "geometry": value["geometry"]
                }
        }
        ready[network.compressed] = new
    return ready


def create_cidr(jdata: Dict):
    del jdata["is_put"]
    result = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def put_cidr(jdata: Dict):
    del jdata["is_put"]
    result = requests.put(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def push(ready):
    for value in ready.values():
        try:
            if value["is_put"]:
                put_cidr(value)
            else:
                create_cidr(value)
        except ValueError:
            pass


def main():
    get_bearer()
    probes = get_probes()
    enhanced_probes = enhance(probes)
    records = prepare(enhanced_probes)
    push(records)


if __name__ == "__main__":
    main()
