import socket
from os import environ
from typing import List, Dict, Union
from urllib.parse import urljoin

import requests
from hdx.location.country import Country

api_auth = None


def get_bearer():
    global api_auth
    headers = {'content-type': "application/x-www-form-urlencoded"}
    payload = {
        "grant_type": "client_credentials",
        "client_id": environ.get("GEOIP_AUTH_ID"),
        "client_secret": environ.get("GEOIP_AUTH_SECRET"),
        "audience": environ.get("GEOIP_API_URL")
    }
    api_auth = requests.post(urljoin(environ.get("GEOIP_AUTH_URL"), "oauth/token"), headers=headers, data=payload).json()


def notfound():
    result = requests.get(
        urljoin(environ.get('GEOIP_API_URL'), 'v1.0/metrics'),
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    result.raise_for_status()
    return result.json()["notfound"]


def lookup_cidrs(cidrs: List[str]):
    result = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidrs/internal'),
        json=cidrs,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def clean(not_found: List[Dict[str, Union[str, int]]]):
    result = lookup_cidrs([item["cidr"] for item in not_found])
    for i, item in enumerate(not_found):
        if "error" in result[i]:
            yield item["cidr"]


def parse_whois_line(line):
    cleaned = [part.strip().replace("\r", "").replace("\n", "") for part in line.decode().split(" | ")]
    return cleaned


def check_announcement(cidrs):
    query_list = [cidr.split("/")[0] for cidr in cidrs]
    query_string = "begin\n" + "\n".join(query_list) + "\nend"
    announced = {}
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(("bgp.tools", 43))
        s.sendall(query_string.encode())
        i = len(query_list)
        while i > 0:
            data = s.recv(10240)
            if len(data) == 0:
                break
            result = parse_whois_line(data)
            if result[6] == "ERR_AS_NAME_NOT_FOUND":
                continue
            announced[result[2]] = {
                "cidr": result[2],
                "asn": "AS" + result[0],
                "allocated_cc": result[3],
                "rir": result[4],
                "as-name": result[6][:128]
            }
            i -= 1
    return announced


def fill_blanks(announced: Dict[str, Dict[str, str]]):
    ready = {}
    for key, value in announced.items():
        country = Country.get_country_info_from_iso2(value["allocated_cc"])
        if country:
            value["geo"] = {
            "type": "Feature",
            "properties": {"radius": 0.0},
            "geometry": {
                "type": "point",
                "coordinates": [
                    country.get("#geo+lon", 0),
                    country.get("#geo+lat", 0),
                    ]
                }
            }
            ready[key] = value
    return ready


def create_cidr(jdata: Dict):
    result = requests.post(
        urljoin(environ.get('GEOIP_API_URL'), f'v1.0/cidr/{jdata["cidr"]}'),
        json=jdata,
        headers={"Authorization": f"Bearer {api_auth['access_token']}"})
    return result.json()


def push(ready: Dict[str, Dict[str,str]]):
    for value in ready.values():
        try:
            create_cidr(value)
        except ValueError:
            pass


def main():
    get_bearer()
    not_found = notfound()
    announced = check_announcement(clean(not_found))
    ready = fill_blanks(announced)
    push(ready)


if __name__ == "__main__":
    main()
